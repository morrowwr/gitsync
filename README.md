
# Syncing to `cf2` using `git`

W. Ross Morrow
[wrossmorrow@stanford.edu](mailto:wrossmorrow@stanford.edu)
[web.stanford.edu/~morrowwr](web.stanford.edu/~morrowwr)
[wrossmorrow.com](wrossmorrow.com)

Due to recent security concerns, RSS recently asked DS to close `ssh` access to `cf2`
instances except through the `yen` servers via the `cf2` command line tool. This may 
interfere with existing workflows as, for example, you can't use a `ssh` application
to directly edit files on the `cf2` instance. I, at least, use the `git` tool to organize, 
version control, and sync project files across different machines including my laptop and multiple `cf2` 
instances. 

This brief doc outlines how I do this, and how you could too. After setting everything up, 
syncing between machines comes down to running
```
    me@local$ git add . ; git commit -m "some message" ; git push
```
on your local machine and 
```
    me@remote$ git pull
```
in the `cf2` instance. Pretty easy. 

You need to be (marginally) comfortable working with Unix shells (or the Mac OSX `terminal` application), 
    but if you work on the `yen` servers, with `cf2`, or with `ec2` at all you're already there.

## Basic Setup

Setting things up seems may seem like alot of work, but once you've done it once or twice it 
becomes rote and very fast. For me, doing these steps for a new project only takes a few minutes. 

### `git` 

Obviously, `git` needs to be installed for any of this to work. Good tutorials are at 
<a href="https://git-scm.com/book/en/v1/Getting-Started-Installing-Git" target="_blank">here</a> and
<a href="https://gist.github.com/derhuerst/1b15ff4652a867391f03" target="_blank">here</a>. On Windows
machines I use the `git bash` shell available <a href="http://gitforwindows.org/" target="_blank">here</a>.

There are also lots of great instructions available for <code>git</code>. For example, 
<a href="https://guides.github.com/introduction/git-handbook/" target="_blank">here</a>. Stanford's 
<a href="https://code.stanford.edu" target="_blank">gitlab</a> has a great review at 
<a href="https://code.stanford.edu/help" target="_blank">code.stanford.edu/help</a>. While it is worth 
understanding <code>git</code>, anything you need to understand for this specific purpose should be listed explicitly below.

### Set Up your Local Repo

Let's say you have a project you are working on whose files reside in `~/work/my-project` on your laptop or desktop. The first thing you need to do is initialize a `git` repository (or "repo" as they are usually called). First, move into your project directory:
```
    me@local$ cd ~/work/my-project
```
Then executing 
```
    me@local$ git init
```
creates the datastructures for an "empty" repository in the project directory. You should see something like
```
    Initialized empty Git repository in /Users/me/work/my-project/.git/
```
    in your terminal. This empty repo doesn't
have anything in it; to start tracking files execute
```
    me@local$ git add .
    me@local$ git commit -m "initial commit"
```
What's happened here is you've told `git` to track "everything" in the current directory 
and "committed" changes being tracked (which amounts to adding the files in the current directory). You have
to put in a commit message (like `-m "initial commit"`); if you don't add one like shown `git`
will open up an editor for you to write one.

### Ignoring Stuff

It is often useful to track only <em>some</em> of the content in that directory. To facilitate this, 
    you can add an "ignore" file named `.gitignore` that describes what `git` should
    ignore. For example, if `~/work/my-project` has a sub-folder `~/work/my-project/paper`
    containing documents related to an academic paper about the research, you can tell `git`
to ignore that directory and anything in it by making `.gitignore` be
```
    paper
    paper/*
```
Similarly, you could <em>also</em> tell `git` to ignore any Word docs, LaTex files, or pdf's in the project directory
    with
```
    paper
    paper/*
    *.docx
    *.tex
    *.pdf
```
Using `.gitignore` can be very useful.

### Set Up a Remote Repo

To sync between machines you need an online intermediary. RSS uses <a href="bitbucket.com" target="_blank">bitbucket</a>, 
but you can also use <a href="github.com" target="_blank">github</a> or even Stanford's <a href="code.stanford.edu" target="_blank">gitlab</a>
Choose one of these sites, and create an account (if you haven't already).

I would also recommend creating an ssh key on 
your local machine, using `ssh-keygen` and usually named `~/.ssh/id_rsa` and `~/.ssh/id_rsa.pub`, 
and registering `~/.ssh/id_rsa.pub` with your <a href="bitbucket.com" target="_blank">bitbucket</a>/<a href="github.com" target="_blank">github</a>/<a href="code.stanford.edu" target="_blank">gitlab</a> account. This should give your local machine access
to any repo you set up. Detailed instructions are online (<a href="https://confluence.atlassian.com/bitbucket/set-up-an-ssh-key-728138079.html" target="_blank">bitbucket</a>, <a href="https://help.github.com/articles/adding-a-new-ssh-key-to-your-github-account/" target="_blank">github</a>) and you can add keys in gitlab in your <a href="https://code.stanford.edu/profile/keys" target="_blank">profile settings</a>

While in your account, also create a repository (preferably using the same name as your local repo, 
e.g. `my-project`). You should probably make the repo private instead of public, which means you have to explicitly
specify access rights to be able to use it. All of RSS' repos on <a href="bitbucket.com" target="_blank">bitbucket</a> are
private by default.

### Link the Local and Remote Repos

Each of these sites, <a href="bitbucket.com" target="_blank">bitbucket</a>, <a href="github.com" target="_blank">github</a>, 
and <a href="code.stanford.edu" target="_blank">gitlab</a> make it pretty easy to sync from (or to) your local machine. Usually
instructions are available on the page you get right after creating a repo.

Say you're on <a href="bitbucket.com" target="_blank">bitbucket</a>, 
    and you named your repo `my-project`. Then from your local machine, issuing the commands
```
    me@local$ git remote add origin git@bitbucket.org:myaccount/my-project.git
    me@local$ git push -u origin master
```
will ship the current state of the code to the online repo. You can check the results by clicking on the "source" 
item in the left-hand menu.

If you're just using `git` to sync, you don't have to do anything else.

### Clone the Repo on `cf2` 

Now you have to link the repo into `cf2`. Log onto your instance, via `ssh`, and get into your
 instance using the `cf2` tool. Create a `ssh` key the same way as before, calling 
```
    me@remote$ ssh-keygen
```
and following the instructions. You <em>may</em> also need to run 
```
    me@remote$ ssh-add ~/.ssh/id_rsa
```
to make sure `ssh` knows your "identity" when contacting other machines.

If you type
```
    me@remote$ cat ~/.ssh/id_rsa.pub
```
you'll see the public key itself printed out; something weird like
```
    ssh-rsa AAAAB3NzaC1yd2EAAAADAQABAAABAQC2DyOZe848r7CsNB7aSpiSuygZ0GD/jKblvi5gJEOms2he/
    fIJx/roIRLbVq3nJJVcORbpAkd7uLewYNvmz0tmWn4MDpOwpXwkoKLexbgQ2+Pylm5ygBgVEJCfkl9EPd1EEX
    2Pw9cqS0gXJPZqqsPNMorIG/re+BFDiXNYZWrJrL81X9mYnGH6T8FF6u365eTW2c/HUJJzbA1+ldTB2Us4B9T
    JXqtYZOZOkxRToAdNuAiy0bB8lLE06S32H8wsGvV0Yoi84qyrcEQ89D0Cee8D8ME9isYNdk4aa48uQvYeaLRN
    mSLTSz82cNb2KeaWH6zmdS9i5wsPGc/R5vSTRbkj me@GSB-MYMACHINE
```
Copy the text that got printed out (not literally the text above), as you'll need to put it in the online 
    repository to gain access to the repo.

If you are in <a href="bitbucket.com" target="_blank">bitbucket</a> and in your repo, click on "Settings" 
    followed by "Access Keys" and "Add Key". You'll see a place to add a key, which you can name as you like. After pasting the key text
    you can click save and close the window.

After you have added the key online to gain access, you can return to your `cf2` terminal 
    and execute 
```
    me@remote$ git clone git@bitbucket.org:myaccount/my-project.git
```
This will "clone" or copy the repo into a directory `my-project` in whatever folder you are currently in.

### Cloning the Repo Somewhere Else 

If you want the repo somewhere else, follow the steps from the `ssh` key generation onwards. I often have a copy of 
a project repo a few places.

## While You're Working

Syncing while you're working is pretty easy. I typically edit files on my local machine, and have a terminal 
application open with two windows or tabs: one in the local project folder, and another in the mirrored folder
on the `cf2` instance. After making changes to some part of your code, do
```
    me@local$ git add . ; git commit -m "some message" ; git push
```
in the local machine terminal and then
```
    me@remote$ git pull
```
in the `cf2` instance terminal. I usually do this once in each terminal, 
    and then just use the "up" arrow key to repeat commands in the terminal. That's all you need to sync! 


If I am also actively working in the `cf2` instance with edited code, I will open another terminal into the 
`cf2` instance for running the actual commands I need. If, for example, the code is for `matlab`, 
I'll open up a terminal on `cf2`, start `matlab`, and `cd` into the project directory.
After syncing with `git pull` in the other terminal, any updated `matlab` can be run immediately.


If you want some extra convenience, you can write a simple `bash` script to wrap the `add-commit-push`
commands on your local machine. If you're on Mac OSX or Unix, put the following script
```
    #!/bin/bash
    M="no message"
    if [ "$#" -ne 0 ] ; then M=$( echo $1 ); fi
    git add . 
    git commit -m "$M"
    git push
```
in a file named `gitpush.sh`, run `chmod a+x gitpush.sh` to make it executable, and then
    run 
```
    me@local$ ./gitpush.sh "my commit message"
```
whenever you need to commit and push to the online repo.

## Summary

Besides lots of other benefits, `git` is a useful tool to facilitate two-line, syncing between 
local and remote machines. I find this process simple and useful, and hope you do too.

---

![GSB CIRCLE Logo](GSB_CIRCLE_H_2C.png?raw=true)
